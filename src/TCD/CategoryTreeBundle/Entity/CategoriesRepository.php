<?php
/**
 * @author: Dima <dmitrij.rysanow@netteam.pl>
 * @date: 30.11.14
 * @time: 16:14
 */

namespace TCD\CategoryTreeBundle\Entity;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class CategoriesRepository extends NestedTreeRepository
{

    /**
     * get whole tree array
     * @return array tree
     */
    public function getTree()
    {
        $result = array();
        $roots = $this->getRootNodes();
        foreach ($roots as $root) {
            $result[] = $this->childrenHierarchy($root);
        }
        return $result;
    }
} 