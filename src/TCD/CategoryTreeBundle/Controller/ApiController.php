<?php

namespace TCD\CategoryTreeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use TCD\CategoryTreeBundle\Entity\Categories;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use TCD\CategoryTreeBundle\Entity\Activity;


class ApiController extends Controller
{
    private $format = "json";
    private $serializer;

    public function __construct()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $getSetMethodNormalizer = new GetSetMethodNormalizer();

        $normalizers = array($getSetMethodNormalizer);

        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * Get only root nodes
     *
     * @param Request $request
     * @return Response root nodes collection
     */
    public function getRootNodesAction(Request $request)
    {
        $this->format = $request->get("format");

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");

        $nodes = $repo->getRootNodes();
        $data = array();
        foreach ($nodes as $node) {
            $data[] = $this->getNeededParams($node);
        }

        $response = $this->format($data);
        $this->storeActivity($request);
        return new Response($response);
    }

    /**
     * Get whole tree
     *
     * @param Request $request
     * @return Response
     */
    public function wholeTreeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");

        $tree = $repo->findAll();

        $this->format = $request->get("format");

        foreach ($tree as $node) {
            $arr[] = $this->getNeededParams($node);
        }

        $response = $this->format($arr);
        $this->storeActivity($request);
        return new Response($response);
    }

    /**
     * Get single node
     *
     * @param Request $request
     * @return Response
     */
    public function getNodeAction(Request $request)
    {
        $this->format = $request->get("format");

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");

        $id = $request->get('id');
        $node = $repo->find($id);
        if ($node instanceof Categories) {
            $response = $this->format($this->getNeededParams($node));
            $this->storeActivity($request);
            return new Response($response);
        } else {
            throw $this->createNotFoundException("Node not found");
        }
    }

    /**
     * add node to tree
     * POST params:
     * parent: id of parent node (optional - if null it will be root node)
     * name: name of added node
     *
     * @param Request $request
     * @return Response - id of newly created node
     */
    public function addNodeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $parentId = $request->request->get('parent', null);
        if (null !== $parentId) {
            $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");
            $parent = $repo->find($parentId);
        } else {
            $parent = null;
        }
        $name = $request->request->get('name');

        $category = new Categories();
        $category->setName($name);
        $category->setParent($parent);

        $em->persist($category);
        $em->flush();

        $this->storeActivity($request);
        return new Response($category->getId());
    }

    /**
     * Remove node and it's children
     *
     * @param Request $request
     * @return Response
     */
    public function removeNodeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");

        $id = $request->get('id');
        $node = $repo->find($id);
        if ($node instanceof Categories) {
            $em->remove($node);
            $em->flush();
        } else {
            throw $this->createNotFoundException("Node not found");
        }

        $this->storeActivity($request);
        return new Response("success");
    }

    /**
     * Edit name of selected node
     * POST params:
     * id - id of edtited node
     * name - new name
     *
     * @param Request $request
     * @return Response success if success
     */
    public function editNodeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository("TCDCategoryTreeBundle:Categories");
        $id = $request->get('id');
        $node = $repo->find($id);

        if ($node instanceof Categories) {
            $name = $request->request->get('name');
            $node->setName($name);
            $em->persist($node);
            $em->flush();
        } else {
            throw $this->createNotFoundException("Node not found");
        }

        $this->storeActivity($request);
        return new Response("success");
    }

    /**
     * format array to selected syntax
     *
     * @param array $data
     * @return JsonResponse or Response with XML depends of $this->format
     */
    private function format($data)
    {
        return $this->serializer->serialize($data, $this->format);
    }

    /**
     * Creates array with needed in view data. Use it for get JSTree plugin work properly
     *
     * @param Categories $category
     * @return array
     */
    private function getNeededParams(Categories $category)
    {
        $data = array(
            "id" => $category->getId(),
            "text" => $category->getName()
        );

        if ($category->getParent() instanceof Categories) {
            $data["parent"] = $category->getParent()->getId();
        } else {
            $data["parent"] = "#";
        }

        return $data;
    }

    /**
     * Stores query, userAgent and datetime of request in DB
     *
     * @param Request $request
     */
    private function storeActivity(Request $request)
    {
        $userAgent = $request->headers->get('User-Agent');
        $query = $request->getRequestUri();
        $datetime = new \DateTime();
        $em = $this->getDoctrine()->getManager();

        $activity = new Activity();
        $activity->setUserAgent($userAgent);
        $activity->setDatetime($datetime);
        $activity->setUrl($query);

        $em->persist($activity);
        $em->flush();
    }
}
