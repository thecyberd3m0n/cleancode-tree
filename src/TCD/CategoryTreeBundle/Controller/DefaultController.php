<?php

namespace TCD\CategoryTreeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TCDCategoryTreeBundle:Default:index.html.twig');
    }
}
