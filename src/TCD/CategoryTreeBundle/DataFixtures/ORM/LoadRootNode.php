<?php
/**
 * @author: Dima <dmitrij.rysanow@netteam.pl>
 * @date: 04.12.14
 * @time: 11:47
 */

namespace TCD\CategoryTreeBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TCD\CategoryTreeBundle\Entity\Categories;

class LoadRootNode implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $rootNode = new Categories();

        $rootNode->setName("root");

        $manager->persist($rootNode);
        $manager->flush();
    }
} 