<?php
/**
 * @author: Dima <dmitrij.rysanow@netteam.pl>
 * @date: 01.12.14
 * @time: 16:58
 */

namespace TCD\CategoryTreeBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use TCD\CategoryTreeBundle\Entity\Categories;

class CategoriesListener {

    /**
     * Will delete all childrens of deleten node
     *
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();
        $repo = $entityManager->getRepository("TCDCategoryTreeBundle:Categories");

        $children = $repo->findBy(array("parent" => $entity->getId()));
        foreach ($children as $child) {
            $entityManager->remove($child);
        }
    }
}