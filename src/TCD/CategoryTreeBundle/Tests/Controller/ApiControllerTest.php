<?php

namespace TCD\CategoryTreeBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Config\Definition\Exception\Exception;
use TCD\CategoryTreeBundle\Entity\Categories;

class ApiControllerTest extends WebTestCase
{
    const entityName = "test";

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    /**
     * @var Categories
     */
    private $testEntity;

    public function setUp()
    {
        $this->setupEntityManager();
        $this->testEntity = new Categories();
        $this->testEntity->setName(self::entityName);
        $this->em->persist($this->testEntity);
        $this->em->flush();

        parent::setUp();
    }

    public function testRemovenode()
    {
        $client = static::createClient();

        $client->request('GET', '/api/removeNode/' . $this->testEntity->getId());
        $client->request('GET', '/api/getNode.json/' . $this->testEntity->getId());

        //after deleting entity response to request on it ID should be code 404
        $this->assertEquals(404, $client->getResponse()->getStatusCode());

    }

    public function testAddRootNode()
    {
        $client = static::createClient();

        $parameters = array(
            "name" => 'otherEntity'
        );

        $client->request('POST', '/api/addNode', $parameters);
        $id = $client->getResponse()->getContent();

        $repo = $this->em->getRepository("TCDCategoryTreeBundle:Categories");
        $category = $repo->find($id);
        if ($category instanceof Categories) {
            $this->em->remove($category);
            $this->assertEquals($id, $category->getId());
        } else {
            throw new Exception("Node not added");
        }
    }

    public function testRootNodeExisting()
    {
        if ($this->testEntity instanceof Categories) {
            $this->assertEquals($this->testEntity->getName(), self::entityName);
        }
    }

    public function testWholetree()
    {
        $client = static::createClient();

        $client->request('GET', '/api/wholeTree.json');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetnode()
    {
        $client = static::createClient();
        $id = $this->testEntity->getId();
        $client->request('GET', '/api/getNode.json/' . $id);
        $this->assertEquals(200, $client->getResponse()->getStatusCode()); //200 - category found
    }

    public function tearDown()
    {
        if ($this->testEntity instanceof Categories) {
            $this->em->remove($this->testEntity);
        }
        $this->em->flush();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function setupEntityManager()
    {
        $kernel = static::createKernel();
        $kernel->boot();

        $this->container = $kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
    }
}
