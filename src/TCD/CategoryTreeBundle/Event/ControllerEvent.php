<?php
/**
 * @author: Dima <dmitrij.rysanow@netteam.pl>
 * @date: 02.12.14
 * @time: 11:44
 */

namespace TCD\CategoryTreeBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class ControllerEvent extends Event
{
    /**
     * Requirement about saving requested URL, datetime and user-agent
     *
     * @param Request $request
     */
    public function logActivity(Request $request)
    {

    }
} 