/**
 * Created by tcd-linux on 02.12.14.
 */

$(function () {


    $('#tree').jstree({
        "themes": {"stripes": true},
        'core': {
            'check_callback': true,
            'data': {
                'url': function (node) {
                    var addr;
                    if (node.id === "#") {
                        addr = '/api/wholeTree.json';
                    } else {
                        addr = '/api/getNode.json/' + node.id;
                    }
                    return addr;
                },
                'dataType': 'JSON',
                'data': function (node) {
                    //console.log(node);
                }
            }
        },
        "plugins": [
            "contextmenu","plugins","ui"
        ],

        "contextmenu": {
            items: customMenu
        }

    });

    $("button#create-root-node").click(function() {
        createNode(null);
    });

    /**
     * Creates the node under clicked
     *
     * @param int parent
     */
    function createNode(parent)
    {
        var parId = parent;

        $("#create-form").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            buttons: {
                "Cancel": function() {
                    $("#create-form").dialog("close");
                },
                "Create": function() {
                    var name = $("#name").val();
                    var toSend = {
                        "name": name
                    }

                    if (parId !== null) {
                        toSend.parent = parId;
                    }
                    $.ajax({
                        type: "POST",
                        url: "/api/addNode",
                        data: toSend,

                        success: function (data) {
                            $("#create-form").dialog("close");
                            $("#message").html('Added node id: <b>'+ data + '</b>');
                            $("#dialog-message").dialog({
                                modal: true,
                                buttons: {
                                    Ok: function() {
                                        $( this ).dialog( "close" );
                                    }
                                }
                            });
                            $('#tree').jstree("refresh");
                        }
                    });
                }
            }
        }).dialog("open");

    }

    /**
     * CustomMenu params for JSTree plugin
     *
     * @param node
     * @returns {{renameItem: {label: string, action: Function}, deleteItem: {label: string, action: Function}, createItem: {label: string, action: Function}}}
     */
    function customMenu(node)
    {
        var nodeId = node.id;
        var items = {
            renameItem: { // The "rename" menu item
                label: "Rename",
                action: function (obj) {
                    renameNode(nodeId);

                }
            },
            deleteItem: { // The "delete" menu item
                label: "Delete",
                action: function (obj) {
                    deleteNode(nodeId);

                }
            },
            createItem: { // The "delete" menu item
                label: "Create sub-node",
                action: function (obj) {

                    createNode(nodeId);
                }
            }
        };

        return items;
    }

    /**
     * Prompts and removes node
     * @param id of node to delete
     */
    function deleteNode(id)
    {
        var nodeId = id;
        $("#delete-nodeID").text(id);
        $("#delete-prompt").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            buttons: {
                "Cancel": function() {
                    $("#delete-prompt").dialog("close");
                },
                "Delete": function() {
                    $.ajax({
                        type: "GET",
                        url: "/api/removeNode/"+id,
                        success: function (data) {
                            $("#delete-prompt").dialog("close");
                            if (data==="success") {
                                $("#message").html('Deleted node id: <b>' + id + '</b>')
                                $("#dialog-message").dialog({
                                    modal: true,
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                });
                            } else {
                                $("#delete-prompt").dialog("close");
                                $("#message").html('Something went wrong :( please contact me at dmitrij.rysanow@gmail.com');
                                $("#dialog-message").dialog({
                                    modal: true,
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                });
                            }

                            $('#tree').jstree("refresh");
                        }
                    });
                }
            }
        }).dialog("open");
    }

    /**
     * Prompts and rename node
     *
     * @param id
     */
    function renameNode(id)
    {
        $("#rename-message").html('Renaming node id: <b>' + id + '</b>');

        var nodeId = id;

        $("#dialog-rename").dialog({
            autoOpen: false,
            height: 300,
            width: 350,
            modal: true,
            buttons: {
                "Cancel": function() {
                    $(this).dialog("close");
                },
                "Rename": function() {
                    var name = $("input#new-name").val();
                    var toSend = {
                        "id": nodeId,
                        "name": name
                    }
                    $.ajax({
                        type: "POST",
                        url: "/api/editNode",
                        data: toSend,

                        success: function (data) {
                            $("#dialog-rename").dialog("close");
                            $("#dialog-message").html('Renamed node id: <b>' + id + '</b> to '+ name);
                            $("#dialog-message").dialog({
                                modal: true,
                                buttons: {
                                    Ok: function() {
                                        $( this ).dialog( "close" );
                                    }
                                }
                            });
                            $('#tree').jstree("refresh");
                        }
                    });
                }
            }
        }).dialog("open");
    }
});
